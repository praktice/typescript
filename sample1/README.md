# TypeScript

A strict language typing for JavaScript/ECMAScript; Yet it includes
many more features.

Get Started: https://www.typescriptlang.org/docs/home.html

## Installation

Please install **globally**

```sh
npm i -g typescript
```

## Basic Console Use

If you were to run a `*.ts` file in your terminal; This will turn it into a JavaScript file:

```sh
tsc filename.ts
```

You can see an example in the `/hello` folder.

However, I'd read the docs :)
