// --------------------------------
// Step 1: Basic Types
// --------------------------------
const burger: string = 'Cheesey Burger';
const calories: number = 565;
const isTasty: boolean = true;

function getEnergy(food: string, energy: number): string {
  return `The ${food} gives you ${energy} energy points`;
}

getEnergy(burger, 5021)

// --------------------------------
// Step 2: Tuples
// --------------------------------

let stringStr: [string, string];
stringStr = ['hey', 'f'];

let strNum: [string, number];
strNum = ['yeah', 123];

let strArr: [string, Array<string>];
strArr = ['hey', ['hi', 'ho']];

// --------------------------------
// Step 2: Interfaces
// --------------------------------
interface Food {
  name: String,
  energy: number
}

function getEnergy2(food: Food): String {
  return `The ${food.name} gives you ${food.energy} energy points`;
}

// This must match the Interface "Food",
// because its passed in getEnergy2(food: Food)
const icecream = {
  name: 'Icecream',
  energy: 2000
}
getEnergy2(icecream);


// --------------------------------
// Step 3: Classes
// --------------------------------

class Menu {
  items: Array<string>;
  pages: number;

  constructor(itemList: Array<string>, totalPages: number) {
    this.items = itemList;
    this.pages = totalPages;
  }

  list(): void {
    console.log('Menu for the day:');
    for (let i = 0; i < this.items.length; i++) {
      console.log(` - ${this.items[i]}`);
    }
  }
}

const mondayMenu = new Menu(
  [
    'pancakes',
    'waffles',
    'juice',
  ],
  1
);

// --------------------------------
// Step 4: Generic Types
// The <T> means "generic<T>ype"
// --------------------------------

function getName<T>(argument: string): String {
  return `Your name is ${argument}`;
}

function genericFunc<T>(argument: T): T[] {
  const arrayOfT: T[] = []; // Empty Array for T arg.
  arrayOfT.push(argument);
  return arrayOfT;
}

// Re-use custom functions interestingly
const arrayFromString = genericFunc<string>("Beep");
const arrayFromNumber = genericFunc<number>(22);
