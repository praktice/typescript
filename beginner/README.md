# Learning

First install typescript. TSLint is like
ESLint, it uses a file `tslint.json` to
validate code -- I copied `tslint.json` from an already existing one.

Install it globally via Node, and test it is working:
```sh
npm i -g typescript tslint
tsc -v
```

## Compiling

We can compile TypeScript without much effort just via CLI. I would recommend using VSCode, however.

```sh
tsc file.ts

# CLI Watch for Updates:
tsc file.ts --watch
```

## Pushing to GCR

```sh
# Not sure if i can have the last /example-node?  [!]
docker tag codezeus/example-node us.gcr.io/jesse-apps/codezeus/example-node

docker push us.gcr.io/jesse-apps/codezeus/example-node

docker push us.gcr.io/jesse-apps/codezeus/example-node:with_tag
```
